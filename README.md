# DEPRECATED

# decanter

Currently Crossref API client and OAI-PMH harvester.

## Crossref to Datacite Citation Pusher

Implemented in `decanter.citations`:

    lein run -m decanter.citations
	
### Environment variables

OAI-PMH:

- `OAI_URL`
- `OAI_BEARER_TOKEN`

AWS / DynamoDB:

- `AWS_SECRET_KEY`
- `AWS_ACCESS_KEY`
- `AWS_DYNAMODB_URL`

Lagotto:

- `LAGOTTO_URL`
- `LAGOTTO_AUTH_TOKEN`
- `LAGOTTO_SOURCE_TOKEN`
