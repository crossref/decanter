FROM clojure:lein-alpine

COPY . /app
WORKDIR /app

RUN lein deps
