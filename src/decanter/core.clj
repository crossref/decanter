(ns decanter.core
  (:gen-class)
  (:require [decanter.oai.harvest :as h]
            [decanter.oai.action :as a]
            [decanter.oai.protocol :as p]
            [decanter.affiliations.compare :as ac]
            [decanter.crossref :as cr]
            ;[decanter.page :as page]
            [environ.core :refer [env]]))

;; 1. Run over XML, convert to xml zippers
;; 2. Parse out and check for flags such as has-refs, bad-author
;; 3. Convert to [doi, ownerprefix, memberid, flags...]
;;               [doi, owernprefix, memberid, authorposition, authorflags... ]
;;               [doi, ownerprefix, memberid, funderposition, funderflags... ]
;;               etc...
;;               or
;;               [doi, op, mid, flags..., count-author, count-author-with-orcid,
;;                has-author-with-orcid (maybe a 1 for agg)] etc
;; 4. Page samples, same:
;;    [doi, op, mid, pageflags... ]
;; 5. Aggregate to mid, prefix levels

;; Graph per member of article count to author count

(defn do-harvest []
  (let [crossref-oai (p/conf (env :oai-url)
                             :bearer-token (env :oai-bearer-token)
                             :force-url true
                             :type "cr_unixsd")]
    (h/harvest crossref-oai
               (partial a/dump-id-to-file "ids.txt")
               :selection {:set "J"})))

(defn do-affiliation-comparison []
  (let [results (atom {})]
    {:results results
     :future (future (ac/compare-affiliations "affiliations.log" results))}))

(defn- main [& args]
  ())

