(ns decanter.lagotto
  (:require [org.httpkit.client :as hc]
            [clojure.data.json :as json]
            [clj-time.format :as df]
            [slingshot.slingshot :refer [throw+]]))

(defn- request-headers [conf]
  (cond-> {"Content-Type" "application/json"}
    (:auth-token conf) (assoc "Authorization" (str "Bearer " (:auth-token conf)))))

(defn- request-params [conf params] params)

(defn- make-request
  ([conf resource]
   (make-request conf resource {} :get))
  ([conf resource params]
   (let [path (str (:url conf) "/" (name resource))
         query-params (request-params conf params)
         headers (request-headers conf)
         {:keys [error body status]}
         @(hc/get path {:headers headers :query-params query-params})]
     (if (or error (not (#{200 201 202} status)))
       (throw+ {:type ::http-error
                :lagotto-config conf
                :query-params query-params
                :resource resource
                :path path
                :headers headers
                :status status
                :error error
                :body body})
       (json/read-str body :key-fn keyword)))))

(defn- post-message [conf resource message]
  (let [path (str (:url conf) "/" (name resource))
        headers (request-headers conf)
        {:keys [error body status]}
        @(hc/post path {:headers headers :body (json/write-str message)})]
    (when (or error (not (#{200 201 202} status)))
      (throw+ {:type ::http-error
               :lagotto-config conf
               :headers headers
               :resource resource
               :path path
               :post-body message
               :error error
               :body body
               :status status}))))

(defn- make-ids-request [conf kind]
  (->> (make-request conf kind)
       kind
       (map :id)
       (map keyword)))

(defn conf [url & {:keys [auth-token source-token source-id license]}]
  (cond-> {:url url}
    license (assoc :license license)
    source-id (assoc :source-id source-id)
    source-token (assoc :source-token source-token)
    auth-token (assoc :auth-token auth-token)))

(defn events [conf & {:keys [source-token]}]
  (let [st (or source-token (:source-token conf))]
    (make-request conf :events {:source_token st})))

(defn event [conf uuid]
  (make-request conf :events {:uuid uuid}))

(defn relation-type-ids [conf] (make-ids-request conf :relation_types))

(defn source-ids [conf] (make-ids-request conf :sources))

(defn work-type-ids [conf] (make-ids-request conf :work_types))

(defn relations [conf pid]
  (->> (str "works/" pid "/references")
       (make-request conf)
       :references))

(defn register-relation [conf subject-pid relation-type-id object-pid
                         occurred-at & {:keys [event-uuid]}]
  (let [uuid (or event-uuid (str (java.util.UUID/randomUUID)))
        date-format (:date-time-no-ms df/formatters)]
    (post-message
     conf
     :events
     {:id uuid
      :message_action "create"
      :license (:license conf)
      :source_token (:source-token conf)
      :occurred_at (df/unparse date-format occurred-at) 
      :subj_id subject-pid
      :obj_id object-pid
      :relation_type_id (name relation-type-id)
      :source_id (-> conf :source-id name)})
    uuid))
   
