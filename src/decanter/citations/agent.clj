(ns decanter.citations.agent
  "Process for periodically loading cross-RA citations into a Lagotto
   instance."
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clojure.string :as str]
            [org.httpkit.client :as hc]
            [clojure.data.zip.xml :as xml]
            [clojure.data.zip :refer [children-auto]]
            [taoensso.timbre :as log]
            [crossref.log :as clog]
            [clj-time.core :as dt]
            [clj-time.format :as df]
            [cronj.core :as cronj]
            [slingshot.slingshot :refer [throw+ try+]]
            [taoensso.faraday :as far]
            [environ.core :refer [env]]
            [decanter.crossref :as crossref]
            [decanter.lagotto :as lagotto]
            [decanter.datacite :as datacite]
            [decanter.unixsd :as unixsd]
            [decanter.oai.harvest :as oaih]
            [decanter.oai.protocol :as oaip]
            [slingshot.slingshot :refer [try+]]))

;; Data storage
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn pk [citing-doi cited-doi] (str citing-doi "_" cited-doi))

(defn create-tables [conf]
  (far/ensure-table conf :pushed [:id :s]
                    {:throughput {:read 1 :write 1}
                     :block? true})
  (far/ensure-table conf :unpushed [:id :s]
                    {:throughput {:read 1 :write 1}
                     :block? true}))

(defn remember-citation
  "Record a citation between one DOI and another that should later
   be pushed to a lagotto instance."
  [conf & {:keys [cited-doi citing-doi]}]
  (log/info "Found reference from" citing-doi "to" cited-doi)
  (when
      (and
       (nil? (far/get-item conf :pushed {:id (pk citing-doi cited-doi)}))
       (nil? (far/get-item conf :unpushed {:id (pk citing-doi cited-doi)})))
    (let [event-uuid (str (java.util.UUID/randomUUID))]
      (log/info "Reference from" citing-doi "to" cited-doi "not known, inserting")
      (far/put-item
       conf
       :unpushed
       {:id (pk citing-doi cited-doi)
        :event-uuid event-uuid
        :cited-doi cited-doi
        :citing-doi citing-doi})
      (log/info "Inserted" citing-doi "to" cited-doi "with event UUID" event-uuid))))

(defn remember-successful-push 
  "Update a previously recorded citation to say that it has been successfully
   pushed to a lagotto instance."
  [conf & {:keys [cited-doi citing-doi event-uuid lagotto-conf]}]
  (far/put-item
   conf
   :pushed
   {:id (pk citing-doi cited-doi)
    :cited-doi cited-doi
    :citing-doi citing-doi
    :event-uuid event-uuid
    :pushed-to (:url lagotto-conf)
    :pushed-at (df/unparse (df/formatters :date-time) (dt/now))})
  (far/delete-item conf :unpushed {:id (pk citing-doi cited-doi)}))

(defn some-unpushed-citations
  "Retrieve a list of so far unpushed citations."
  [conf]
  (far/scan conf :unpushed))

;; Citations to DOIs filtered by prefix
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn doi-prefix [s] (first (str/split s #"/")))

(defn record-filtered-citations [filter-prefixes dynamodb-conf record]
  (let [citing-doi (-> record :id (str/replace #"info:doi/" ""))
        cited-dois (-> record :metadata unixsd/cited-dois)
        filtered-cited-dois (filter
                             #(some #{(doi-prefix %)} @filter-prefixes)
                             cited-dois)]
    (doseq [cited-doi filtered-cited-dois]
      (remember-citation dynamodb-conf
                         :citing-doi citing-doi
                         :cited-doi cited-doi))))

;; Lagotto push
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn issued-date [doi]
  (apply dt/date-time
         (-> (crossref/work doi)
             (get-in [:issued :date-parts])
             first)))

(defn push-citation [dynamodb-conf citing-doi cited-doi event-uuid]
  (try+
    (let [lagotto-conf (lagotto/conf
                        (env :lagotto-url)
                        :auth-token (env :lagotto-auth-token)
                        :source-token (env :lagotto-source-token)
                        :source-id :crossref
                        :license "https://doi.org/10.13003/CED-terms-of-use")
          uuid (lagotto/register-relation lagotto-conf
                         (str "https://doi.org/" citing-doi)
                         :references
                         (str "https://doi.org/" cited-doi)
                         (issued-date citing-doi)
                         :event-uuid event-uuid)]
      (remember-successful-push dynamodb-conf
                                :citing-doi citing-doi
                                :cited-doi cited-doi
                                :event-uuid uuid
                                :lagotto-conf lagotto-conf))
    (catch Object e
      (log/error "Lagotto push failed" e))))

;; Task definitions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn do-update-datacite-prefixes
  "Refresh the datacite prefix list."
  [dt {:keys [datacite-prefixes] :as opts}]
  (log/info "Downloading datacite prefix list")
  (reset! datacite-prefixes (datacite/datacite-prefixes)))

(defn do-pull-oai
  "Pull down unixsd via oai-pmh, store citations to datacite DOI prefixes."
  [dt {:keys [datacite-prefixes dynamodb-conf oai-conf start-override] :as opts}]
  (let [today (->> (dt/now)
                   (df/unparse (df/formatters :date)))
        start-date (or
                    start-override
                    (->> (dt/minus (dt/now) (dt/days 1))
                         (df/unparse (df/formatters :date))))]
    (log/info "Starting harvest for period" start-date "to" today)
    (oaih/harvest oai-conf
                  (partial record-filtered-citations
                           datacite-prefixes
                           dynamodb-conf)
                  :selection {:set "J" :from start-date :until today})
    (oaih/harvest oai-conf
                  (partial record-filtered-citations
                           datacite-prefixes
                           dynamodb-conf)
                  :selection {:set "B" :from start-date :until today}
                  :strategy :single-set)
    (oaih/harvest oai-conf
                  (partial record-filtered-citations
                           datacite-prefixes
                           dynamodb-conf)
                  :selection {:set "S" :from start-date :until today})))

(defn do-push-citations
  "Push previously stored citations to lagotto."
  [dt {:keys [dynamodb-conf] :as opts}]
  (log/info "Pushing unpushed citations")
  (doseq [unpushed-citation (some-unpushed-citations dynamodb-conf)]
    (log/info "Pushing" (:citing-doi unpushed-citation)
              "references" (:cited-doi unpushed-citation))
    (push-citation dynamodb-conf
                   (:citing-doi unpushed-citation)
                   (:cited-doi unpushed-citation)
                   (:event-uuid unpushed-citation))))

(defn -main [& args]
  (clog/prepare-logging
   {:app-name "decanter"
    :component-name "references-agent"
    :component-version "1.0.0"})
  (let [dynamodb-conf {:access-key (env :aws-access-key)
                       :secret-key (env :aws-secret-key)
                       :endpoint (env :aws-dynamodb-url)}
        oai-conf (oaip/conf (env :oai-url)
                            :force-url true
                            :bearer-token (env :oai-bearer-token)
                            :type "cr_unixsd")
        datacite-prefixes (atom nil)
        task-opts {:datacite-prefixes datacite-prefixes
                   :oai-conf oai-conf
                   :dynamodb-conf dynamodb-conf}]
    (log/info "Ensuring dynamodb tables exist...")
    (create-tables dynamodb-conf)
    (log/info "Done ensuring dynamodb tables.")
    (log/info "Updating datacite prefixes for the first time...")
    (do-update-datacite-prefixes (dt/now)
                                 {:datacite-prefixes datacite-prefixes})
    (log/info "Done first update of datacite prefixes.")
    (-> (cronj/cronj :entries
               [{:id       "pull-oai"
                 :handler  do-pull-oai
                 :schedule "0 10 0 * * * *"
                 :opts task-opts}
                {:id       "push-citations"
                 :handler  do-push-citations
                 :schedule "0 /30 * * * * *"
                 :opts task-opts}
                {:id       "update-datacite-prefixes"
                 :handler  do-update-datacite-prefixes
                 :schedule "0 0 0 * * * *"
                 :opts task-opts}])
        (cronj/start!))))
      
