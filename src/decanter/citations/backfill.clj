(ns decanter.citations.backfill
  (:require [clojure.java.io :refer [reader]]
            [clojure.string :as str]
            [taoensso.timbre :as log]
            [decanter.datacite :as datacite]
            [decanter.lagotto :as lagotto]
            [decanter.oai.protocol :as oaip]
            [decanter.citations.agent :as ca]
            [clj-time.core :as dt]
            [environ.core :refer [env]]
            [crossref.log :as clog]
            [slingshot.slingshot :refer [try+]]))

(defn do-backfill []
  (let [oai-conf (oaip/conf (env :oai-url)
                            :force-url true
                            :bearer-token (env :oai-bearer-token)
                            :type "cr_unixsd")
        dynamodb-conf {:access-key (env :aws-access-key)
                       :secret-key (env :aws-secret-key)
                       :endpoint (env :aws-dynamodb-url)}
        a-long-time-ago "2000-01-01"
        datacite-prefixes (datacite/datacite-prefixes)]
    (log/info "Performing full OAI-PMH download...")
    (ca/create-tables dynamodb-conf)
    (ca/do-pull-oai (dt/now) {:datacite-prefixes (atom datacite-prefixes)
                              :oai-conf oai-conf
                              :dynamodb-conf dynamodb-conf
                              :start-override a-long-time-ago})))

(defn -main [& args]
  (clog/prepare-logging
   {:app-name "decanter"
    :component-name "references-backfill"
    :component-version "1.0.0"})
  (do-backfill))
