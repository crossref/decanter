(ns decanter.datacite
  (:require [clojure.data.json :as json]
            [org.httpkit.client :as http]))

(defn datacite-prefixes
  "List of DataCite DOI prefixes"
  []
  (let [params {:facet true
                :facet.field "prefix"
                :facet.limit -1
                :q "*:*"
                :rows 0
                :start 0
                :wt "json"}]
    (let [{:keys [body]} @(http/get "http://search.datacite.org/api"
                                    {:query-params params})]
      (->>
       (-> body
           (json/read-str :key-fn keyword)
           (get-in [:facet_counts :facet_fields :prefix]))
       (partition 2)
       (map first)
       set))))
