(ns decanter.orcid
  (:require [org.httpkit.client :as hc]
            [slingshot.slingshot :refer [throw+]]
            [clj-time.core :as t]
            [clojure.data.zip.xml :as xml]
            [clojure.string :as str]
            [decanter.xml :refer [str->doc]]))

(def api-version "v1.2")

(def api-path (partial str "http://pub.orcid.org/" api-version "/"))

(defn- id-component [orcid-uri]
  (last (str/split orcid-uri #"\/")))

(defn- make-request [path]
  (let [{:keys [body status error]} (-> path name api-path hc/get deref)]
    (if (or error (not= 200 status))
      (throw+ {:type ::bad-response
               :path path
               :status status
               :error error
               :body body})
      (str->doc body))))

(defn- from-date-parts [date-loc]
  (when date-loc
    (->> [(xml/xml1-> date-loc :year xml/text)
          (xml/xml1-> date-loc :month xml/text)]
          ;(xml/xml1-> date-loc :day xml/text)]
         (map #(when-not (nil? %) (Integer/parseInt %)))
         (filter (complement nil?))
         (apply t/date-time))))

(defn- affiliation [affiliation-loc]
  {:name       (xml/xml1-> affiliation-loc :organization :name xml/text)
   :type       (xml/xml1-> affiliation-loc :type xml/text)
   :start-date (-> affiliation-loc (xml/xml1-> :start-date) from-date-parts)
   :end-date   (-> affiliation-loc (xml/xml1-> :end-date) from-date-parts)})

(defn- affiliations [kind orcid]
  (let [doc (make-request (str (id-component orcid) "/orcid-profile"))]
    (map affiliation (xml/xml-> doc
                                 :orcid-profile :orcid-activities
                                 :affiliations :affiliation))))

(def employment (partial affiliations :employment))
(def education (partial affiliations :education))
      
