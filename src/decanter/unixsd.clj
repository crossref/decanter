(ns decanter.unixsd
  (:require [clojure.data.zip.xml :as xml]
            [clojure.data.zip :refer [children-auto]]))

(defn parent-item-loc
  "Returns location to the top-level metadata item. Something like a
   journal or book."
  [loc]
  (xml/xml1-> loc
             :crossref_result :query_result :body :crossref_metadata
             :doi_record :crossref children-auto))

(defn child-item-loc
  "Returns location of the child metadata item, often a journal
   article, conference paper, or data set."
  [metadata-loc]
  (xml/xml1-> metadata-loc children-auto))

(defn parent
  "Returns information about the parent item."
  [parent-loc]
  ())

(defn child
  "Returns information about the child item."
  [child-loc]
  ())

(defn cited-dois
  "The DOIs a record cites."
  [loc]
  (xml/xml-> loc
             :crossref_result :query_result :body :crossref_metadata
             :doi_record :crossref
             children-auto children-auto
             :citation_list :citation :doi xml/text))

(defn titles
  "Titles of the primary item represented by the unixsd. Returns a list
   of {:title :type} where type is :long, :short or :original."
  [item-loc]
  (xml/xml-> item-loc :title))

(defn awards [funding-loc]
  ())

(defn funding
  [item-loc]
  (xml/xml-> item-loc :fundref))

(defn licenses
  [item-loc]
  ())

(defn citations
  [item-loc]
  ())

(defn item
  [loc]
  (let [parent-item-loc (parent-item-loc loc)
        child-item-loc (child-item-loc parent-item-loc)]
    (-> (child child-item-loc)
        (assoc :titles (titles child-item-loc))
        (assoc :citations (citations child-item-loc))
        (assoc :funders (funding child-item-loc))
        (assoc :licenses (licenses child-item-loc))
        (assoc :container (parent parent-item-loc)))))


