(ns decanter.affiliations.compare
  "Compare quantity of affiliations between Crossref and ORCID."
  (:require [decanter.crossref :as crossref]
            [decanter.orcid :as orcid]
            [clojure.core.memoize :as m]
            [slingshot.slingshot :refer [try+]]
            [clj-time.core :as t]))

(defn from-date-parts [date-time-object]
  (when-let [parts (get-in date-time-object [:date-parts])]
    (apply t/date-time (first parts))))

(defn doi-author-records
  "For a DOI record, return a number of mini records, each representing
   a DOI, author pair."
  [record]
  (map 
   #(hash-map :doi (:DOI record)
              :title (-> record :title first)
              :issn (:ISSN record)
              :published-print (-> record :published-print from-date-parts)
              :published-online (-> record :published-online from-date-parts)
              :name (str (:given %) " " (:family %))
              :orcid (:ORCID %)
              :affiliations-from-doi (map :name (:affiliation %)))
   (:author record)))

(defn orcid-affiliations
  "All affiliations and their dates of applicability for an ORCID."
  [orcid]
  (try+
   (orcid/employment orcid)
   (catch Object e
     (spit "errors.log" (str (pr-str e) "\n") :append true))))

(def orcid-affiliations-lru (m/lru orcid-affiliations :lru/threshold 1000))

(defn applicable-date?
  "Is target-date within date range [optional-start-date, optional-end-date]?
   If optional-start-date is nil, only end date is considered. Same for
   optional-start-date. If both bounding dates are nil, returns true."
  [target-date optional-start-date optional-end-date]
  (cond (nil? target-date)
        false
        (and (not (nil? optional-start-date))
             (not (nil? optional-end-date)))
        (t/within? optional-start-date optional-end-date target-date)
        (not (nil? optional-start-date))
        (t/after? target-date optional-start-date)
        (not (nil? optional-end-date))
        (t/before? target-date optional-end-date)
        :else
        true))

(defn earliest-date
  "Like clj-time.core/earliest except ignores nils rather than considering them
   an earilest possible date."
  [& dates]
  (->> dates
       (filter (complement nil?))
       (sort-by identity #(if (t/after? %1 %2) 1 -1))
       first))

(defn applicable-orcid-affiliation?
  "Does the DOI-author record have ORCID-sourced affiliations that match
   the earliest publication date?"
  [doi-author-record]
  (->> (map applicable-date?
            (repeat (earliest-date (:published-online doi-author-record)
                                   (:published-print doi-author-record)))
            (map :start-date (:affiliations-from-orcid doi-author-record))
            (map :end-date (:affiliations-from-orcid doi-author-record)))
       (filter true?)
       empty?
       not))

(defn update-affiliation-comparison [results doi-author-record]
  (let [applicable-from-doi (-> doi-author-record :affiliations-from-doi empty? not)
        applicable-from-orcid (applicable-orcid-affiliation? doi-author-record)]
    (cond-> results
      applicable-from-doi
      (update-in [:applicable-doi-affiliation-count] (fnil inc 0))
      applicable-from-orcid
      (update-in [:applicable-orcid-affiliation-count] (fnil inc 0))
      (and applicable-from-doi applicable-from-orcid)
      (update-in [:applicable-both-affiliation-count] (fnil inc 0))
      true
      (update-in [:count] (fnil inc 0)))))

(defn compare-affiliations [log-file result-state]
  (let [doi-records (crossref/works {:filter {:has-orcid true}})]
    (doseq [doi-author-record (pmap #(->> %
                                          :orcid
                                          orcid-affiliations-lru
                                          (assoc % :affiliations-from-orcid))
                                    (->> doi-records
                                         (mapcat doi-author-records)
                                         (filter :orcid)))]
      (swap! result-state
             update-affiliation-comparison
             doi-author-record)
      (spit log-file (str (pr-str doi-author-record) "\n") :append true))))
 
