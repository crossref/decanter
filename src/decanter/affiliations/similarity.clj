(ns decanter.affiliations.similarity
  "Produce similarity scores for affiliation names in Crossref/ORCID and
   their potential matches in OrgRef, PSI, GRID."
  (:require [clojure.data.csv :as csv]
            [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.core.memoize :as m]
            [clojure.string :as str]
            [clojure.set :as set]
            [clojure.edn :as edn]
            [clj-fuzzy.metrics :as metrics]
            [korma.core :as k]
            [korma.db :as db]
            [decanter.api :as api]))

;; todo improve scoring by looking at word occurence frequency?
;; todo look at applying matching to geographic location
;;        for now, take the penultimate affilation component
;;        e.g.
;;        "Department of Chemistry; York University; Toronto ON Canada"
;;        -> "York University"

(defn split-words [s]
  (-> s
      str/lower-case
      (str/replace #"[\.,\(\);\:\-\/\\]" " ")
      str/trim
      (str/split #"\s+")))

(defn ngrams-up-to [n words]
  (concat
   ;; (mapcat (partial partition n 1) words)
   (partition n 1 words)
   (when (> n 1) (ngrams-up-to (- n 1) words))))

(defn org-entry [id id-type label]
  (let [words (split-words label)]
    {:id id
     :type id-type
     :name label
     :words words
     :ngrams (set (ngrams-up-to 4 words))}))

(defn add-org-entry [dictionary o]
  (doseq [word (:words o)]
    (swap! dictionary update-in [word] #(when (or (nil? %) (< (count %) 100))
                                          ((fnil conj []) % o)))))

(defn load-orgref [csv-file name-dictionary]
  (with-open [rdr (io/reader csv-file)]
    (doseq [line (drop 1 (csv/read-csv rdr))]
      (->> (org-entry (nth line 10) :orgref (nth line 0))
           (add-org-entry name-dictionary)))))

(defn load-psi [db-config name-dictionary]
  (db/with-db db-config
    (doseq [row (k/select
                 "psiorgs"
                 (k/fields ["PSIOrgCode" :psi-id]
                           ["psistandardname" :name-1]))]
                           ;; Ignore alias names for now
                           ;; ["Alias1" :name-2]
                           ;; ["Alias2" :name-3]
                           ;; ["Alias3" :name-4]))]
      ;; (let [psi-names (->> [(:name-1 row) (:name-2 row)
      ;;                       (:name-3 row) (:name-4 row)]
      ;;                      (remove nil?))]
      ;;   (doseq [name psi-names]
      ;;     (->> (org-entry (:psi-id row) :psi name)
      ;;          (add-org-entry name-dictionary)))))))
      ;; Ignore single word PSI names
      (when (> (count (split-words (:name-1 row))) 1)
        (->> (org-entry (:psi-id row) :psi (:name-1 row))
             (add-org-entry name-dictionary))))))

(defn load-grid [grid-json-file name-dictionary]
  (let [doc (-> grid-json-file slurp (json/read-str :key-fn keyword))
        no-redirected-orgs (filter #(= (:status %) "active") (:institutes doc))]
    (doseq [org no-redirected-orgs]
      (doseq [org-name (concat [(:name org)] (:aliases org))]
        (->> (org-entry (:id org) :grid org-name)
             (add-org-entry name-dictionary))))))

;; todo take into account word occurence frequency
;; todo take into account words that are in the target but not the match
;;        those should reduce the overall score
;;        ( multiply score by inverse of missing words / total words in match ? )
(defn match-score-a [words o]
  (let [matching-ngrams (set/intersection (set (:ngrams o))
                                          (set (ngrams-up-to 4 words)))
        max-score (reduce #(+ %1 (count %2)) 0 (:ngrams o))]
    (if (zero? max-score)
      0
      (/
       (reduce #(+ %1 (count %2)) 0 matching-ngrams)
       max-score))))

(defn match-score-b [words o]
  (let [target-ngrams (set (ngrams-up-to 4 words))
        matching-ngrams (set/intersection (:ngrams o) target-ngrams)
        max-score (* (reduce #(+ %1 (count %2)) 0 (:ngrams o))
                     (max 1 (reduce #(+ %1 (count %2)) 0
                                    (set/difference target-ngrams (:ngrams o)))))]
    (if (zero? max-score)
      0
      (/
       (reduce #(+ %1 (count %2)) 0 matching-ngrams)
       max-score))))

(def stop-words (set ["institute" "hospital"
                      "university" "of" "department" "and"
                      "national" "school" "center" "medical"
                      "city" "county" "health" "public"
                      "research" "sciences"]))

(defn best-match-ngrams [name-dictionary target-str]
  (let [words (split-words target-str)
        check-words (set/difference (set words) stop-words)
        matching-orgs (reduce #(concat %1 (get @name-dictionary %2))
                              []
                              check-words)]
    ;; (doseq [w check-words]
    ;;   (println (str w " " (count (get @name-dictionary w)))))
    ;; (println (str "Org list size " (count matching-orgs)))
    (->> matching-orgs
         (map #(vector (match-score-a words %) %))
         (sort-by first)
         last)))
     
(defn best-match-jaro-winkler [name-dictionary target-str]
  (let [discovered (->> @name-dictionary
                        keys
                        (map #(vector (metrics/jaro-winkler % target-str) %))
                        (sort-by first)
                        last)]
    (merge (get @name-dictionary (second discovered))
           {:score (first discovered) :name (second discovered)})))

(defn matching-context []
  (let [grid-dictionary   (atom {})
        orgref-dictionary (atom {})
        psi-dictionary    (atom {})
        grid-matcher      (m/lru (partial best-match-ngrams grid-dictionary)
                                 :lru/threshold 100)
        orgref-matcher    (m/lru (partial best-match-ngrams orgref-dictionary)
                                 :lru/threshold 100)
        psi-matcher       (m/lru (partial best-match-ngrams psi-dictionary)
                                 :lru/threshold 100)]
    (load-grid "grid.json" grid-dictionary)
    (load-orgref "orgref.csv" orgref-dictionary)
    (korma.config/set-delimiters "`")
    (load-psi (db/mysql {:db "occ_db" :user "root" :password "crossref"
                         :host "localhost" :delimiters "`"})
              psi-dictionary)
    {;:grid-matcher grid-matcher
     ;:orgref-matcher orgref-matcher
     :psi-matcher psi-matcher}))

(defn matches [context target-str]
  (into {} (map #(vector (first %) ((second %) target-str)) context)))

(defn affiliations-from-log-seq [log-file]
  (->> log-file
       io/reader
       line-seq
       (map #(edn/read-string {:default (fn [_ _] nil)} %))
       (mapcat :affiliations-from-doi)
       (map #(let [bits (str/split % #";")]
               (if (= 1 (count bits)) (last bits) (-> bits drop-last last))))
       (map str/trim)))

(defn affiliations-from-api-seq []
  (->> (api/works {:filter {:has-affiliation true
                            :from-created-date "2015"
                            :until-created-date "2015"}})
       (mapcat :author)
       (mapcat :affiliation)
       (map :name)))

(defn affiliations-from-txt-seq [txt-file]
  (line-seq (clojure.java.io/reader txt-file)))

(defn dump-affiliations-to-txt [affiliation-seq]
  (doseq [a affiliation-seq]
    (spit "affiliations.txt" (str a "\n") :append true)))

(defn take-primary []
  (doseq [a (line-seq (clojure.java.io/reader "affiliations-uniq.txt"))]
    (let [bits (str/split a #";")
          primary (if (= 1 (count bits)) (last bits) (-> bits drop-last last))]
      (spit "affiliations-primary.txt" (str primary "\n") :append true))))
  
(defn dump-matches-to-csv [mc affiliations-seq csv-out-file]
  (with-open [w (io/writer csv-out-file)]
    (csv/write-csv
     w
     (->> affiliations-seq
          (pmap #(let [ms (matches mc %)]
                   [%
                    ;; (-> ms :grid-matcher first ((fnil double 0)))
                    ;; (-> ms :grid-matcher second :id)
                    ;; (-> ms :grid-matcher second :name)
                    (-> ms :psi-matcher first ((fnil double 0)))
                    (-> ms :psi-matcher second :id)
                    (-> ms :psi-matcher second :name)]))))))
                    ;; (-> ms :orgref-matcher first ((fnil double 0)))
                    ;; (-> ms :orgref-matcher second :id)
                    ;; (-> ms :orgref-matcher second :name)]))))))

(defn count-full-scores [csv-file]
  (with-open [rdr (io/reader csv-file)]
    (reduce
     #(if (= (-> %2 second Double/parseDouble) 1.0)
        (inc %1)
        %1)
     0
     (csv/read-csv rdr))))
