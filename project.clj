(defproject crossref/decanter "0.1.0-SNAPSHOT"
  :description ""
  :url "http://decanter.labs.crossref.org"
  
  :main ^:skip-aot decanter.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :dev {:plugins [[lein-dotenv "1.0.0"]]}}

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :jvm-opts ["-Xmx1g" "-server"] 

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [com.taoensso/encore "2.91.1"]
                 [crossref/log-clj "1.0.0"]
                 [clj-time "0.9.0"]
                 [im.chit/cronj "1.4.4"]
                 [clj-webdriver "0.6.1"]
                 [http-kit "2.1.18"]
                 [org.clojure/core.cache "0.6.4"]
                 [org.clojure/core.memoize "0.5.8"]
                 [org.clojure/core.async "0.2.374"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/data.csv "0.1.3"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/data.zip "0.1.1"]
                 [org.clojure/tools.logging "0.3.1"]
                 [robert/bruce "0.8.0"]
                 [clj-fuzzy "0.3.1"]
                 [korma "0.4.2"]
                 [amazonica "0.3.48"]
                 [slingshot "0.12.2"]
                 [environ "1.0.0"]
                 [com.taoensso/faraday "1.9.0"]
                 [mysql/mysql-connector-java "5.1.38"]])
